<?php

namespace App\Helpers;
use Request;
use App\Model\Log as LogModel; 


class Log
{
    public static function log($type,$msg,$json=null) 
    {
        $log = [];
        $log['type'] = $type;
        $log['message'] = $msg;
        $log['json'] = $json;
        $log['url'] = Request::fullUrl();
        $log['method'] = Request::method();
        $log['ip'] = Request::ip();
        $log['agent'] = Request::header('user-agent');
        LogModel::create($log);
    }


    public static function logActivityLists()
    {
        return LogModel::latest()->get();
    }


}