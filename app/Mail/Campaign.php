<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Campaign extends Mailable
{
    use Queueable, SerializesModels;

    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    { 

        // dd($this->content);
        // dd($this->content);
        // dd($this->content);
        // return $this->markdown('emails.custom-markdown');
        return $this->from('noreply@sharmag.in')
                ->subject('Testing Mails')
                ->markdown('emails.campaign')
                ;
                //->markdown('emails.campaign');
                // ->view('emails.campaign');   
        // return $this->markdown('emails.campaign');
    }
}
