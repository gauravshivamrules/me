<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class List extends Model 
{
	// public $table = "equipments";

	//protected $guarded = ['user_id','name','loc_order','comments'];

	public function subscribersCount() {
	    return $this->hasMany(Subscriber::class);
	}     
}