<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'type','message','json', 'url', 'method', 'ip', 'agent'
    ];
}
