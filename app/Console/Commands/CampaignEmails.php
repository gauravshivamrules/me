<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\Campaign;
use App\Helpers\Log;
use Mail;
use DB;

class CampaignEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:campaign_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails to campaign subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() 
    {
        $items = DB::table('campaign_emails')->whereStatus(0)->get(); 
        // dd($items);
        if($items){
            foreach ($items as $userEmail) {
                try { 
                    // Mail::to($userEmail->user_email)->send(new Campaign($userEmail->content)); 
                    Mail::send('emails.campaign',['content'=>$userEmail->content],function($msg) use($userEmail) {
                        $msg->to($userEmail->user_email)
                            ->subject($userEmail->subject)
                            ->from($userEmail->from_email,$userEmail->from_name)
                            ->replyTo($userEmail->reply_to_email, $name = null);
                    });

                    Log::log('CampaignEmail','mail sent to: '.$userEmail->user_email);
                    DB::table('campaign_emails')->whereId($userEmail->id)->update([
                        'status'=>1,
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                } catch(Exception $e) {
                    Log::log('CampaignEmailException','server returned: '.$e->getMessage());
                    // dd($e->getMessage());
                }
            }
        }   
    }
}
