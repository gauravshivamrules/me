<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;



class UserController extends Controller
{
    

    public function getDashboard() {
        $subscribers = DB::table('subscribers')->count();
        $list = DB::table('lists')->count();
        $campaigns = DB::table('campaigns')->count();
    	return view('users.dashboard',compact('subscribers','list','campaigns'));
    }

    public function postLogin(Request $request) {
    	$this->validate($request,[
    			'email'=>'required|email',    
    			'password'=>'required'
    		],[
    			'email.required'=>__('Email is required.'),
    			'email.email'=>__('Enter Valid Email.'),
    			'password.required'=>__('Password is required.')
    		]
    	);
    	if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
    	    return redirect()->route('getDashboard');
    	}
    	return redirect()->back()->with('error','Sorry bad credentials');
    }

    public function logout() {
    	Auth::logout();
    	return redirect('/');
    }


}
