<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;
// use App\Model\List; 
// use App\Model\List;
use App\Model\Subscriber;

class SubscriberController extends Controller
{
    public function getLists() {
    	return view('subscribers.index');
    }

    public function getListsAjax() {
        $lists = DB::table('lists')->get();
    	return Datatables::of($lists)
    				->addColumn('action',function($l){
    					$str= "<a href='javascript:;' onclick='editList(".$l->id.")' class='btn btn-success'><i class='fas fa-edit'></i></a>";
    				    $str .="&nbsp;<a href='#' onclick='deleteList(".$l->id.")' class='btn btn-danger'><i class='fas fa-trash'></i></a>";
                        return $str;
                    })
                    ->addColumn('scount',function($l){
                        return  DB::table('subscribers')->whereListId($l->id)->count();
                    })
    				->escapeColumns([])
    				->make(true); 
    }

    public function addList(Request $request) {
        $response['success']=false;
        $add = DB::table('lists')->insertGetId([
            'title'=>$request->title,
            'description'=>$request->description,
            'created_at'=>date('Y-m-d H:i:s'),
            'updated_at'=>date('Y-m-d H:i:s')
        ]);
        if($add){
            $subscribers = explode("\n", $request->subscribers);
            if($subscribers){
                foreach ($subscribers as  $s) {
                    DB::table('subscribers')->insert([
                        'list_id'=>$add,
                        'email'=>$s,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);
                }
            }
            $response['success']=true;
            $response['msg']='List added';
        }
        echo json_encode($response);die;
    }
    public function deleteList($id) {
        if($id){
            if(DB::table('lists')->whereId($id)->delete()){
                return redirect()->back()->with('success','List deleted');
            }
            return redirect()->back()->with('error','Sorry,list could not be deleted,please try again!');

        }
        return redirect()->back()->with('error','Invalid request');
    }

    public function getEditList($list) {
        $response['success']=false;
        if($list){
            $data = DB::table('lists')->whereId($list)->first();
            if($data){
                $response['data'] = $data;
                $response['subscribers'] = DB::table('subscribers')->whereListId($list)->pluck('email','id');
                $response['success']=true;
            }
        }
        echo json_encode($response);die;
    }

    public function postEditList(Request $request) {
        // dd($request->all());
        if($request->has('id') && $request->id){
            $update = DB::table('lists')->whereId($request->id)->update([
                'title'=>$request->title,
                'description'=>$request->description,
                'updated_at'=>date('Y-m-d H:i:s')
            ]); 
            if($update){
                $checkCount = DB::table('subscribers')->whereListId($request->id)->count();
                if($checkCount){
                    $delete = DB::table('subscribers')->whereListId($request->id)->delete();
                }
                
                
                $subscribers = explode("\n", $request->subscribersEdit);
                if($subscribers){
                    foreach ($subscribers as  $s) {
                        DB::table('subscribers')->insert([
                            'list_id'=>$request->id,
                            'email'=>$s, 
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ]);
                    }
                    return redirect()->back()->with('success','List updated!');
                }
                
            } 
            return redirect()->back()->with('error','Sorry, list could not updated!');  
        }
        return redirect()->back()->with('error','Sorry, Invalid Request!');  
    }

}
