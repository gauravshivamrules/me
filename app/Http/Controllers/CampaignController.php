<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\Mail\Campaign;
use App\Helpers\Log;

use Mail;
use DB;



class CampaignController extends Controller
{
   	
   	public function sendTestCampaign($id){
   		if($id){
   			$content = DB::table('campaigns')->whereId($id)->first();
   			if($content){
               // dd($content);
   				// if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
   					try {
   						Mail::to('gauravshivamrules@gmail.com')->send(new Campaign($content)); 
   						/*Mail::send('emails.campaign',['content'=>$content],function($msg){
   							$msg->to('gauravshivamrules@yahoo.in')->subject('Test');
   						});*/
   						dd('Mail sent');    
   					} catch(Exception $e) {
   						dd($e->getMessage());
   					}
   				// }
   			}
   		}	
   	}

   	public function campaignPreview($id) {
   		if($id){
   			$content = DB::table('campaigns')->whereId($id)->first();
            //dd($content);
   			if($content){
   				return view('emails.campaign',compact('content'));
   			}
   		}
   	}

	public function getCampaign() {
		return view('campaigns.index');
	}
	public function getCampaignAjax() {
		$camps = DB::table('campaigns')->select('campaigns.id','campaigns.subject','lists.title','campaigns.created_at')
			->leftJoin('lists','lists.id','=','campaigns.list_id')
			->get();
		return Datatables::of($camps)
					->addColumn('action',function($l){
                  $str='';
						// $str= "<a href='#' class='btn btn-success'><i class='fas fa-edit'></i></a>";
					   	$str .="&nbsp;<a href='/campaigns/preview/".$l->id."'  class='btn btn-info'><i class='fas fa-eye'></i></a>";
					    // $str .="&nbsp;<a href='#' onclick='deleteList(".$l->id.")' class='btn btn-danger'><i class='fas fa-trash'></i></a>";
					   
	                 return $str;
	                })
	                ->editColumn('created_at',function($d){
	                    return date('d-M-Y H:i',strtotime($d->created_at));
	                })
					->escapeColumns([])
					->make(true); 
	}
   	public function getCampaignAdd() {
   		$list = DB::table('lists')->pluck('title','id');
   		return view('campaigns.add',compact('list'));
   	}
   	public function postCampaignAdd(Request $request) {
   		// dd($request);
   		$campaign = DB::table('campaigns')->insertGetId([
   			'subject'=>$request->subject,
   			'from_name'=>$request->from_name,
   			'from_email'=>$request->from_email,
   			'reply_to_email'=>$request->reply_to_email,
   			'content'=>$request->html_content,
   			'list_id'=>$request->list,
   			'created_at'=>date('Y-m-d H:i:s'),
   			'updated_at'=>date('Y-m-d H:i:s')
   		]);
   		if($campaign){ 
            Log::log('postCampaignAdd','new campaign added by user:'.Auth::user()->email);
            $this->putInQueue($request->all(),$campaign);
   			return redirect('/campaigns')->with('success','Campaign added and its being sent in background!');
   		}
   		return redirect()->back()->with('error','Campaign could not be added,please try again!');
   	}

      public function putInQueue($data,$campaign) {
         $subscribers = DB::table('subscribers')->whereListId($data['list'])->get();
         if($subscribers){
            foreach ($subscribers as $sub) { 
               DB::table('campaign_emails')->insert([
                  'campaign_id'=>$campaign,
                  'user_email'=>trim($sub->email),
                  'from_name'=>$data['from_name'],
                  'from_email'=>$data['from_email'],
                  'reply_to_email'=>$data['reply_to_email'],
                  'subject'=>$data['subject'],
                  'content'=>$data['html_content'],
                  'created_at'=>date('Y-m-d H:i:s'),
                  'updated_at'=>date('Y-m-d H:i:s')
               ]);
            }
         }
      }


}
