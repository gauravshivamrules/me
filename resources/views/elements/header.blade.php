<nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
           
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                  <?php echo __('Hi'); ?>, {{ Auth::user()->name}}
                <span class="caret"></span>
              </a>
              <!-- style="background: #222d32;" -->
                <ul class="dropdown-menu">
                  
                  <!-- <li><a href="/users/change_password"><i class="fa fa-lock"></i><?php echo __('Change Password'); ?></a></li> -->
                    <li><a href="/logout"><i class="fa fa-key"></i><?php echo __('Logout'); ?></a></li>
                
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
          </ul>
        </div>
      </nav>