@extends('layouts.back')
@section('title','|Mass Email')
@section('content')
<style type="text/css">
   .card {
   position: relative;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #fff;
   background-clip: border-box;
   border: 0 solid rgba(0,0,0,.125);
   border-radius: .25rem;
   }
</style>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<div class="container">
      <div class="row" id="step1">
         <div class="col-md-12">
            <div class="card card-secondary">
               <div class="card-header">
                  <h3 class="card-title">All Lists</h3>
                  <a href="#" data-toggle="modal" data-target="#myModal" style="float: right;" class="btn btn-secondary"> <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
               </div>
               <div class="card-body">
                  <table id="listsTable" class="table">
                     <thead>
                        <th>Title</th>
                        <th>Subscribers Count</th>
                        <th>Action</th>
                     </thead>
                     <tbody>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" id="addForm">
         @csrf
      <div class="modal-header">
          <h4 class="modal-title">Add New List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
             <div class="form-group">
                <label for="exampleInputEmail1">Subject</label>
                <input type="text" name="title" class="form-control" id="title">
             </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea class="form-control" name="description"></textarea>
             </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Subscribers</label>
                <textarea class="form-control" name="subscribers"></textarea>
             </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="submitbtn" class="btn btn-secondary">Add</button>
      </div>
      </form>

    </div>

  </div>
</div>

<div id="myModalEdit" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" id="editForm" action="/lists/edit" >
         @csrf
      <div class="modal-header">
          <h4 class="modal-title">Update List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body">
             <div class="form-group">
                <label for="exampleInputEmail1">Subject</label>
                <input type="text" name="title" class="form-control" id="titleEdit">
                <input type="hidden" name="id" id="id">

             </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea class="form-control" name="description" id="descriptionEdit"></textarea>
             </div>
             <div class="form-group">
                <label for="exampleInputPassword1">Subscribers</label>
                <textarea class="form-control" name="subscribersEdit" id="subscribersEdit"></textarea>
             </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" id="submitbtn" class="btn btn-secondary">Update</button>
      </div>
      </form>

    </div>

  </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    createTable();
    
    $('#submitbtn').click(function(){
      $.ajax({
         type:'post',
         url:'/lists/add',
         dataType:'json',
         data:$('#addForm').serializeArray(),
         success:function(r){
            console.log(r);
            if(r.success){
              // alert('list added');
              $(document).Toasts('create', {
                class: 'bg-success',
                title: r.msg
              })
              $('#myModal').modal('hide');
              createTable();
            }
         }
      }); 
    });
  });

 

  function createTable() {
    $('#listsTable').DataTable({ 
           responsive: true,  
           processing: true,  
           serverSide: true, 
           bDestroy:true,
           ajax: "/lists-ajax",      
           lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
           columns: [ 
               { data: 'title', name: 'title' },
               { data: 'scount', name: 'scount' },
               { data: 'action', name: 'action'}        
           ] 
    });
  }

  
   function deleteList(id) {
      if(id){
        if(confirm('Are you sure you want to delete this list?')){
          window.location.href ='/lists/delete/'+id;  
        }
        return false;
      }
   } 

   function editList(list) {
    if(list){
      $.ajax({
        url:'/lists/edit/'+list,
        dataType:'json',
        success:function(r){
          if(r.success){
            $('#id').val(r.data.id); 
            $('#titleEdit').val(r.data.title);
            $('#descriptionEdit').val(r.data.description);
            if(r.subscribers){
              let subs ='';
              $.each(r.subscribers,function(i,v){
                subs +=v +"\n";
              })
              $('#subscribersEdit').val(subs);  
            }
            $('#myModalEdit').modal('show');  
          }
          
        }
      });
     
    }
   }
</script>

@endsection