@extends('layouts.back')
@section('title','|Mass Email')
@section('content')
<style type="text/css">
   .card {
   position: relative;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #fff;
   background-clip: border-box;
   border: 0 solid rgba(0,0,0,.125);
   border-radius: .25rem;
   }
</style>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap-steps.min.css">
<link href="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="container">
   <form method="post">
      @csrf
      <div class="row" id="step1">
         <!-- left column -->
         <div class="col-md-4">
            <div class="card card-primary">
               <div class="card-header">
                  <h3 class="card-title">New Recipient</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <div class="card-body">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Name</label>
                     <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter Subject">
                  </div>
                  
                  <div class="form-group">
                     <label for="exampleInputFile">From Email</label>
                     <input type="email" name="from_email"  class="form-control" id="from_email" placeholder="Enter From Email">
                  </div>
                
               </div>
               <!-- /.card-body -->
               <div class="card-footer">
                  <button type="button" name="submit" id="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="card card-success">
               <div class="card-header">
                  <h3 class="card-title"> </h3>
               </div>
               <div class="card-body">
                  <textarea class="form-control" id="html_content"></textarea>
               </div>
               <!-- /.card-body -->
            </div>
         </div>
      </div>
      <div class="row" id="step2" style="display: none;">
         <div class="row" id="step1">
            <!-- left column -->
            <div class="col-md-4">
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Select Recipients</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <div class="card-body">

                  </div>
               </div>
            </div>

            <div class="col-md-8">
               <div class="card card-primary">
                  <div class="card-header">
                     <h3 class="card-title">Preview Campaign</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <div class="card-body">

                  </div>
               </div>
            </div>

         </div>
      </div>
   </form>
</div>
<script type="text/javascript">
   $('#submit').click(function(){
      $('#step1').hide();
      $('#step2').show();
   });
   $('#html_content').summernote();
</script>
@endsection