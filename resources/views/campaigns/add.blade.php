@extends('layouts.back')
@section('title','|Mass Email')
@section('content')
<style type="text/css">
   .card {
   position: relative;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #fff;
   background-clip: border-box;
   border: 0 solid rgba(0,0,0,.125);
   border-radius: .25rem;
   }
</style>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<!-- <link href="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script> -->


<script src="//cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<div class="container">
   <form method="post" action="/campaigns/add">
      @csrf
      <div class="row" id="step1">
         <!-- left column -->
         <div class="col-md-4">
            <div class="card card-secondary">
               <div class="card-header">
                  <h3 class="card-title">New Campaign</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <div class="card-body">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Subject</label>
                     <input type="text" name="subject" class="form-control" id="subject" placeholder="Enter Subject" required>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputPassword1">From Name</label>
                     <input type="text" class="form-control" id="from_name" name="from_name" placeholder="Enter From Name" required>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputFile">From Email</label>
                     <input type="email" name="from_email"  class="form-control" id="from_email" placeholder="Enter From Email" required>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputPassword1">Reply To Email</label>
                     <input type="email" class="form-control" id="reply_to_email" name="reply_to_email" placeholder="Enter Reply to email" required>
                  </div>
                 <!--  <div class="form-group">
                     <label for="exampleInputPassword1">Plan Text Version</label>
                     <textarea class="form-control" name="plain_text_version"></textarea>
                  </div> -->
               </div>
               <!-- /.card-body -->
               <div class="card-footer">
                  <button type="button" name="submit" id="submit" class="btn btn-secondary">Next</button>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="card card-secondary">
               <div class="card-header">
                  <h3 class="card-title"> </h3>
               </div>
               <textarea name="html_content" id="html_content"></textarea>
               <div class="card-body">
                  
               </div>
               <!-- /.card-body -->
            </div>
         </div>
      </div>
      <div class="row" id="step2" style="display: none;">
            <div class="col-md-4">
               <div class="card card-secondary">
                  <div class="card-header">
                     <h3 class="card-title">Select Recipients</h3>
                  </div>
                  <div class="card-body">
                     <select class="form-control" name="list" required>
                        <option value="">Select List</option>
                        @foreach($list as $key => $value)
                           <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                     </select>

                  </div>
               </div>
            </div>

            <div class="col-md-8">
               <div class="card card-secondary">
                  <div class="card-header">
                     <h3 class="card-title">Preview Campaign</h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <div class="card-body">
                     <div class="form-group">
                        <label>From:</label><span id="from_prev"></span>
                     </div>
                     <div class="form-group">
                        <label>Subject:</label><span id="subject_prev"></span>
                     </div>
                     <label>Content:</label>
                     <div id="content"></div>
                  </div>
               </div>
            </div>

      </div>
      <div class="row" id="btn" style="display: none;">
         <div class="col-md-12">
            <button type="submit" class="btn btn-secondary">Submit </button>
         </div>
      </div>   
   </form>
</div>
<script type="text/javascript">
   $('#submit').click(function(){
      $('#step1').hide();
      $('#step2').show();
      $('#btn').show();

      $('#subject_prev').html($('#subject').val());
      $('#from_prev').html($('#from_name').val()+'&lt;'+$('#from_email').val()+'&gt;' );
      var myContent = tinymce.get("html_content").getContent();
      $('#content').html(myContent);

   });

   tinymce.init({
       selector: '#html_content',
       height: 300,
       plugins: [
         'advlist autolink link image lists charmap print preview hr anchor pagebreak',
         'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
         'table emoticons template paste help'
       ],
       toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
         'bullist numlist outdent indent | link image | print preview media fullpage | ' +
         'forecolor backcolor emoticons | help',
       
       menubar: 'favs file edit view insert format tools table help'
   });

</script>
@endsection