@extends('layouts.back')
@section('title','|Mass Email')
@section('content')
<style type="text/css">
   .card {
   position: relative;
   display: -webkit-flex;
   display: -ms-flexbox;
   display: flex;
   -webkit-flex-direction: column;
   -ms-flex-direction: column;
   flex-direction: column;
   min-width: 0;
   word-wrap: break-word;
   background-color: #fff;
   background-clip: border-box;
   border: 0 solid rgba(0,0,0,.125);
   border-radius: .25rem;
   }
</style>
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
<div class="container">
      <div class="row" id="step1">
         <div class="col-md-12">
            <div class="card card-secondary">
               <div class="card-header">
                  <h3 class="card-title">All Campaigns</h3>
                  <a href="/campaigns/add"  style="float: right;" class="btn btn-secondary"> <i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
               </div>
               <div class="card-body">
                     
                  <table id="campsTable" >
                    <thead>
                        <th>Subject</th>
                        <th>List</th>
                        <th>Added On</th>
                        <th>Action</th>
                     </thead>
                     <tbody>
                        
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
</div>

<script type="text/javascript" src="//cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    createTable();
  });

 

  function createTable() {
    $('#campsTable').DataTable({ 
           responsive: true,  
           processing: true,  
           serverSide: true, 
           bDestroy:true,
           ajax: "/campaigns-ajax",      
           lengthMenu: [[10, 25, 50,100,-1], [10, 25, 50,100,"All"]],
           columns: [ 
               { data: 'subject', name: 'subject' },
               { data: 'title', name: 'title' },
               { data: 'created_at', name: 'created_at' },
               { data: 'action', name: 'action'}        
           ] 
    });
  }

  
   function deleteList(id) {
      if(id){
        if(confirm('Are you sure you want to delete this list?')){
          window.location.href ='/campaigns/delete/'+id;  
        }
        return false;
      }
   } 
</script>

@endsection