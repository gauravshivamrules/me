<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {return view('users.login');})->name('login');
Route::group(['middleware' => ['auth']], function () {  
	Route::get('/dashboard','UserController@getDashboard')->name('getDashboard');
	Route::get('/logout','UserController@logout')->name('logout');


	Route::get('/campaigns','CampaignController@getCampaign')->name('getCampaign');
	Route::get('/campaigns-ajax','CampaignController@getCampaignAjax')->name('getCampaign');
	Route::get('/campaigns/add','CampaignController@getCampaignAdd')->name('getCampaignAjax');
	Route::post('/campaigns/add','CampaignController@postCampaignAdd')->name('postCampaignAdd');

	Route::get('/campaigns/preview/{id}','CampaignController@campaignPreview')->name('campaignPreview');

	Route::get('/campaigns/send-test/{id}','CampaignController@sendTestCampaign')->name('sendTestCampaign');




	Route::get('/lists','SubscriberController@getLists')->name('getLists');
	Route::get('/lists-ajax','SubscriberController@getListsAjax')->name('getListsAjax');
	Route::post('/lists/add','SubscriberController@addList')->name('addList');
	Route::get('/lists/delete/{id}','SubscriberController@deleteList')->name('deleteList');

	Route::get('/lists/edit/{id}','SubscriberController@getEditList')->name('getEditList');
	Route::post('/lists/edit','SubscriberController@postEditList')->name('postEditList');



});

Route::post('/login','UserController@postLogin')->name('postLogin');
